// Include gulp
var gulp = require('gulp');

// Include plugins
var concat       = require('gulp-concat');
var livereload   = require('gulp-livereload');
var connect      = require('gulp-connect');
var plumber      = require('gulp-plumber');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var rename       = require('gulp-rename');
var replace      = require('gulp-replace');
var cssmin       = require('gulp-cssmin');
var concatCss    = require('gulp-concat-css');
var emojicss     = require('postcss-emoji');

// Live Reload
gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

// HTML files
gulp.task('html', function() {
    gulp.src('*.html')
        .pipe(livereload())
        .pipe(gulp.dest('./dist'));
});

// Compile Sass
gulp.task('clean', function() {
    return gulp.src('*.css')
        .pipe(postcss([ autoprefixer() ]))
        .pipe(postcss([ emojicss() ]))
        .pipe(gulp.dest('./dist'))
        .pipe(livereload());
});

// Watch files for changes
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('style.css', ['clean']);
    gulp.watch('*.html', ['html']);
});


// Default Task
gulp.task('default', ['clean', 'watch', 'connect']);




